# OpenRGB Systemd Services

### For startup and shutdown

This simple package provides a systemd service to run actions on login and logout for OpenRGB, as well as accompanying scripts to edit those files

## Install

Currently Arch is supported, although you can use meson to install manually at your own risk.

To install on Arch, clone this repo and run `makepkg -si`

## Configure

You need to be familiar with openrgb command line interface. Run `openrgb --help` to learn more

1. run `sudo openrgb-systemd-edit-startup` to edit the startup script
2. optionally run `sudo openrgb-systemd-edit-shutdown` to edit the startup script. this may not be necessary as by default this script turns off everything by setting the color to black
3. enable the `openrgb-startup.service` service for your current user with systemd: `systemctl --user enable openrgb-startup.service`
